import 'DataTables.net'
export class Main {
    dataStorage = [];
    isFileUploaded = false;
    constructor() {
        this.createFormatSelectionMenu();

        console.log(document.getElementById("load"))
        console.log($("#container"))

    }

    createFormatSelectionMenu() {
        let containerDiv = document.getElementById("container");
        let dropDownMenu = document.createElement("select");
        dropDownMenu.id = "selectMenu";

        let optionsArray = ["Select a file format...", "Text", "XML", "Google Spreadsheet"];

        for (let i = 0, len = optionsArray.length; i < len; i++) {
            let currentOption = document.createElement("option");
            currentOption.value = optionsArray[i];
            currentOption.innerHTML = optionsArray[i];
            dropDownMenu.append(currentOption);
        }
        let self = this
        dropDownMenu.onchange = function () {
            console.log(self)
            self.generateUploadButton(self)
        };
        containerDiv.appendChild(dropDownMenu);
    }
    generateUploadButton(self) {
        let containerDiv = document.getElementById("container");
        let fileUploadButton = document.createElement("input");
        fileUploadButton.type = "file";
        fileUploadButton.id = "fileToLoad";
        fileUploadButton.innerHTML = "Choose File";
        fileUploadButton.onchange = function (event) {
            self.getTextFromFile(self, event);
        }
        containerDiv.appendChild(fileUploadButton);
    }
    getTextFromFile(self, event) {
        self.isFileUploaded = false;
        self.dataStorage = [];
        let file = event.target.files[0];
        var fileReader = new FileReader();
        fileReader.onload = function (fileLoadedEvent: any) {
            var textFromFileLoaded = fileLoadedEvent.target.result.split(/\r?\n/);
            for (let i = 0, len = textFromFileLoaded.length; i < len; i++) {
                self.dataStorage.push(textFromFileLoaded[i].replace(/\s/g, "").split(','))
            }
            self.isFileUploaded = true;
            self.createTable(self.dataStorage);
        };
        fileReader.readAsText(file);
    }

    validateInputFile(uploadedFile, extension) {
        let validationMarker = `/(.*?)\.(${extension})$/`;
        if (!uploadedFile.match(validationMarker)) {
            alert("Wrong File Extension");
            return false;
        }
    }
    createTable(tableData) {
        var table = document.createElement('table');
        var tableBody = document.createElement('tbody');

        tableData.forEach(function (rowData) {
            var row = document.createElement('tr');

            rowData.forEach(function (cellData) {
                var cell = document.createElement('td');
                cell.appendChild(document.createTextNode(cellData));
                row.appendChild(cell);
            });

            tableBody.appendChild(row);
        });

        table.appendChild(tableBody);
        document.body.appendChild(table);
    }
}